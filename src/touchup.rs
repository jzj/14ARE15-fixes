/*
 * File:    touchup.rs
 * Brief:   New and improved 14ARE15 touchpad workaround!
 *
 * Copyright (C) 2022, 2024 John Jekel
 * See the LICENSE file at the root of the project for licensing info.
 *
*/

/* ------------------------------------------------------------------------------------------------
 * Uses
 * --------------------------------------------------------------------------------------------- */

use std::io::BufRead;
use std::path::Path;
use std::time::Duration;
use std::thread::sleep;

/* ------------------------------------------------------------------------------------------------
 * Constants
 * --------------------------------------------------------------------------------------------- */

const HID_EVENTS_DIR:           &'static str    = "/sys/kernel/debug/hid";
//const I2C_DEVICES_SYSFS_DIR:    &'static str    = "/sys/bus/i2c/devices";
//const I2C_DRIVERS_SYSFS_DIR:    &'static str    = "/sys/bus/i2c/drivers/i2c_hid_acpi";

const TOUCHPAD_I2C_STR:         &'static str    = "i2c-MSFT0001:00";
const TOUCHPAD_SYSFS_DIR:       &'static str    = "/sys/bus/i2c/devices/i2c-MSFT0001:00";
//TODO switch to this or something similar in the future
//const TOUCHPAD_SYSFS_DIR:   &'static str    = concat!(I2C_DEVICES_SYSFS_DIR, "/", TOUCHPAD_I2C_STR);

const I2C_DRIVERS_BIND_PATH:    &'static str    = "/sys/bus/i2c/drivers/i2c_hid_acpi/bind";
const I2C_DRIVERS_UNBIND_PATH:  &'static str    = "/sys/bus/i2c/drivers/i2c_hid_acpi/unbind";
//TODO switch to this or something similar in the future
//const I2C_DRIVERS_BIND_PATH:    &'static str    = concat!(I2C_DRIVERS_SYSFS_DIR, "/bind");
//const I2C_DRIVERS_UNBIND_PATH:  &'static str    = concat!(I2C_DRIVERS_SYSFS_DIR, "/unbind");

const UNBIND_REBIND_LOOP_DELAY_MS: Duration = Duration::from_millis(10);

/* ------------------------------------------------------------------------------------------------
 * Functions
 * --------------------------------------------------------------------------------------------- */

fn main() {
    //Check if we're root
    if !is_root() {
        eprintln!("touchup requires root privileges to function");
        return;
    } else {
        log(5, "Started");
    }

    loop {
        let touchpad_event_filename = find_touchpad_event_filename();
        if let Some(touchpad_event_filename) = touchpad_event_filename {
            block_while_touchpad_okay(&touchpad_event_filename);
            attempt_fix();
        }
    }
}

fn is_root() -> bool {
    //SAFETY: There is no danger at all in calling getuid()
    return unsafe { libc::getuid() } == 0;
}

fn log(severity: u8, message: impl AsRef<str>) {
    eprintln!("touchup: {}", message.as_ref());
    let result = std::fs::write("/dev/kmsg", format!("<{}>touchup: {}", severity, message.as_ref()));
    assert!(matches!(result, Ok(())));
}

fn find_touchpad_event_filename() -> Option<String> {
    //Figure out the HID ID of the touchpad to access the directory by looking in sysfs
    let touchpad_sysfs_dir = std::fs::read_dir(TOUCHPAD_SYSFS_DIR).ok()?;
    for subdir in touchpad_sysfs_dir {
        let subdir_name = subdir.ok()?.file_name();
        let subdir_name = subdir_name.to_str();
        if let Some(subdir_name) = subdir_name {
            let first_character = subdir_name.chars().next();

            //Assume if the subdirectory begins with a 0 it's name corresponds to the HID ID
            if first_character == Some('0') {
                //Give the corresponding debugfs file for the input events
                return Some(HID_EVENTS_DIR.to_owned() + "/" + &subdir_name + "/events");
            }
        }
    }

    None
}

fn wonky_line(line: impl AsRef<str>) -> Option<bool> {//"None" means no judgement about wonkyness
    let line = line.as_ref();
    //dbg!(line);
    if line.contains("report") {
        if line.contains("size 12") {
            return Some(false);//Not wonky
        } else {
            return Some(true);//Wonky
        }
    }
    None
}

fn block_while_touchpad_okay(touchpad_event_filename: impl AsRef<Path>) {
    loop {
        let event_file = std::fs::File::open(touchpad_event_filename.as_ref());
        if let Ok(event_file) = event_file {
            let reader = std::io::BufReader::new(event_file);

            for line in reader.lines() {
                if let Ok(line) = line {
                    if let Some(wonky) = wonky_line(&line) {
                        if wonky {
                            //Touchpad is wonky, return to try to reset it
                            return;
                        }
                    }
                } else {
                    return;//IO error, assume problem and return to try to reset the touchpad
                }
            }
        } else {
            //Else IO error, assume problem and return to try to reset the touchpad
            return;
        }
    }
}

fn attempt_fix() {
    //Unbind
    loop {
        let unbind_result = std::fs::write(I2C_DRIVERS_UNBIND_PATH, TOUCHPAD_I2C_STR.to_string() + "\n");
        
        //Check if we got an error due to "No such device" (so it was sucessfully unbound)
        if let Err(error) = unbind_result {
            if error.raw_os_error().unwrap() == libc::ENODEV {
                break;
            }
        }

        sleep(UNBIND_REBIND_LOOP_DELAY_MS);
    }

    //Bind
    loop {
        let bind_result = std::fs::write(I2C_DRIVERS_BIND_PATH, TOUCHPAD_I2C_STR.to_string() + "\n");

        //Check if we got an error due to "Device or resource busy" (so it was sucessfully rebound)
        if let Err(error) = bind_result {
            if error.raw_os_error().unwrap() == libc::EBUSY {
                break;
            }
        }

        sleep(UNBIND_REBIND_LOOP_DELAY_MS);
    }
}

/* ------------------------------------------------------------------------------------------------
 * Tests
 * --------------------------------------------------------------------------------------------- */

//TODO
