/* touchpad_workaround.rs
 * By: John Jekel
 *
 * Even better Rust reimplementation of touchpad_workaround.c which was a better reimplementation of the original touchpad_workaround.sh script for Aurora
 *
*/

use std::time::Duration;
use std::thread::sleep;

/* Constants */

const TOUCHPAD_I2C_STR: &'static str = "i2c-MSFT0001:00";
const NUM_MINOR_LOOP_TRIES: u64 = 100;
const EVENT_CHECK_TIMEOUT_MS: u64 = 100;
const DELAY_MS: Duration = Duration::from_millis(10);

/* Functions */

fn main() {
    //Check if we're root
    if !is_root() {
        eprintln!("touchpad_workaround requires root privileges to function");
        return;
    } else {
        print(5, "Started");
    }

    //Attempt to fix the touchpad
    let mut i = 0;
    loop {
        print(6, format!("Attempt {}", i + 1));
        if attempt() {
            print(5, format!("Attempt {} successful", i + 1));
            break;
        } else {
            print(3, format!("Attempt {} failed", i + 1));
        }

        sleep(DELAY_MS);

        i += 1;
    }

    //Exit
    print(5, "Finished");
}

fn is_root() -> bool {
    return unsafe { libc::getuid() } == 0;
}

fn print(severity: u8, string: impl AsRef<str>) {
    eprintln!("touchpad_workaround: {}", string.as_ref());
    assert!(matches!(std::fs::write("/dev/kmsg", format!("<{}>touchpad_workaround: {}", severity, string.as_ref())), Ok(())));
}

fn attempt() -> bool {
    print(6, "Beginning unbind loop");
    if attempt_unbind() {
        print(6, "Unbind loop finished");
    } else {
        print(3, format!("Unbind loop failed (couldn't unbind in {} tries)", NUM_MINOR_LOOP_TRIES));
        return false;
    }

    print(6, "Beginning rebind loop");
    if attempt_rebind() {
        print(6, "Rebind loop finished");
    } else {
        print(3, format!("Rebind loop failed (couldn't rebind in {} tries)", NUM_MINOR_LOOP_TRIES));
        return false;
    }

    print(6, "Ensuring touchpad actually works");
    return check_events_to_verify_it_was_sucessful();
}

fn attempt_unbind() -> bool {
    for _ in 0..NUM_MINOR_LOOP_TRIES {
        let result = std::fs::write("/sys/bus/i2c/drivers/i2c_hid_acpi/unbind", TOUCHPAD_I2C_STR.to_string() + "\n");

        //Check if we got an error due to "No such device" (so it was sucessfully unbound)
        if let Err(error) = result {
            if error.raw_os_error().unwrap() == libc::ENODEV {
                return true;
            }
        }
    }

    return false;
}

fn attempt_rebind() -> bool {
    for _ in 0..NUM_MINOR_LOOP_TRIES {
        let result = std::fs::write("/sys/bus/i2c/drivers/i2c_hid_acpi/bind", TOUCHPAD_I2C_STR.to_string() + "\n");

        //Check if we got an error due to "Device or resource busy" (so it was sucessfully rebound)
        if let Err(error) = result {
            if error.raw_os_error().unwrap() == libc::EBUSY {
                return true;
            }
        }
    }

    return false;
}

fn check_events_to_verify_it_was_sucessful() -> bool {
    /* Just because the touchpad was rebound to the driver, dosn't mean it is working (it may be "wonky" or ignoring input)
     * We can however tell by looking at /sys/kernel/debug/SOME_ID/events and parsing the output
     *
     * If when the touchpad moves, it looks like the following, that means it is workingGood looks like this in events:
     * report (size 12) (numbered) =  04 03 88 05 98 06 5c 27 01 88 18 33
     * Digitizers.0056 = 10076
     * Digitizers.ContactCount = 1
     * Button.0001 = 0
     * Digitizers.TipSwitch = 1
     * Digitizers.Confidence = 1
     * Digitizers.ContactID = 0
     * GenericDesktop.X = 1416
     * GenericDesktop.Y = 1688
     * Button.00c5 = 24
     * Button.00c5 = 51
     *
     *
     * The wonky touchpad looks like this in events:
     *
     * report (size 9) (numbered) =  01 00 fc ff 00 00 00 00 00
     * Button.0001 = 0
     * Button.0002 = 0
     * GenericDesktop.X = -4
     * GenericDesktop.Y = -1
     * GenericDesktop.Wheel = 0
     * Consumer.HorizontalWheel = 0
     *
     * Bad shows no events reported at all
     * We can't really tell this condition apart from the user not touching the touchpad, so if there's nothing we just try again after a set timeout
    */

    let mut hid_id: Option<String> = None;

    //Figure out the HID ID of the touchpad to access the directory in debugfs
    let touchpad_sysfs_dir = std::fs::read_dir("/sys/bus/i2c/devices/".to_string() + TOUCHPAD_I2C_STR).unwrap();
    for subdir in touchpad_sysfs_dir {
        let subdir_name = subdir.unwrap().file_name().into_string().unwrap();
        let first_character = subdir_name.chars().next().unwrap();

        //Assume if the subdirectory begins with a 0 it's name corresponds to the HID ID
        if first_character == '0' {
            hid_id = Some(subdir_name);
            break;
        }
    }
    print(6, format!("HID ID is {}", hid_id.as_ref().unwrap()));

    //Read the events file in debugfs
    let (tx, rx) = std::sync::mpsc::channel::<String>();
    std::thread::spawn(
        move || {
            use std::io::BufRead;

            let debugfs_events_file = std::fs::File::open("/sys/kernel/debug/hid/".to_string() + &hid_id.as_ref().unwrap() + "/events")
                                      .expect("It shouldn't take that long to get to this point, so the file should still exist");
            let mut reader = std::io::BufReader::new(debugfs_events_file);

            //Read two lines and discard the first (that is all we need to determine if it is wonky or not)
            let mut line = String::new();
            //Discard the first line
            if matches!(reader.read_line(&mut line), Err(_)) {
                //This means that we didn't read the line in time, and so the next attempt caused this file to disappear
                //So that means we should end this thread
                return;
            }
            line = String::new();
            //Keep the second line
            if matches!(reader.read_line(&mut line), Err(_)) {
                //This means that we didn't read the line in time, and so the next attempt caused this file to disappear
                //So that means we should end this thread
                return;
            }

            //Send it to the main thread
            tx.send(line.trim().to_string()).ok();
        }
    );

    //Try to parse the file, failing if we time out receiving data from the thread, if we failed to parse the file or if the size is wrong
    match rx.recv_timeout(std::time::Duration::from_millis(EVENT_CHECK_TIMEOUT_MS)) {
        Ok(line) => {
            print(6, format!("Event recieved, relevant line is \"{}\"", line));
            //Parse the third string seperated token; if it is 12 then the touchpad is ok, if it is 9 then it is not
            let mut tokenized_line = line.split(&[' ', ')']);
            tokenized_line.next();
            tokenized_line.next();

            if let Some(size_string) = tokenized_line.next() {
                if let Ok(size) = size_string.parse::<usize>() {
                    if size == 12 {
                        print(6, "Got the expected size!");
                        return true;
                    } else {
                        print(6, "Got the wrong size, touchpad is wonky");
                        return false;
                    }
                }
            }

            print(3, "Failed to parse event");
            return false;
        },
        Err(_) => {
            print(3, "Missed timeout, either no user input yet or touchpad isn't working");
            return false;
        }
    }
}
