/* touchpad_workaround.c
 * By: John Jekel
 *
 * Better C reimplementation of original touchpad_workaround.sh script for Aurora
 *
*/

/* Constants And Defines */

#define TOUCHPAD_I2C_STR "i2c-MSFT0001:00\n"
#define NUM_MINOR_LOOP_TRIES 100
#define NUM_MAJOR_LOOP_TRIES 10
#define MINOR_LOOP_DELAY_MS 0
#define MAJOR_DELAYS_MS 0

/* Includes */

#include <assert.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <errno.h>
#include <stdarg.h>
#include <time.h>
#include <unistd.h>

/* Types */

//TODO

/* Variables */

//TODO

/* Static Function Declarations */

static bool is_root(void);
static void print(uint32_t log_level, const char* format_str, ...);
static bool attempt(void);
static void sleep_ms(uint64_t millis);

//TODO perhaps check files and dirs in /sys to ensure it worked instead of just detecting write errors?
//static bool directory_exists(const char* path);
//static bool file_exists(const char* path);

/* Function Implementations */

int main(int argc, char** argv) {
    //Check if we're root
    if (!is_root()) {
        fputs("touchpad_workaround requires root privileges to function\n", stderr);
        exit(1);
    } else
        print(5, "touchpad_workaround: started");

    //TODO before trying check if the touchpad is already working to save time

    //Attempt to fix the touchpad
    for (uint32_t i = 0; i < NUM_MAJOR_LOOP_TRIES; ++i) {
        print(6, "touchpad_workaround: attempt %u", i + 1);
        if (attempt()) {
            print(5, "touchpad_workaround: attempt %u successful", i + 1);
            exit(0);
        } else
            print(3, "touchpad_workaround: attempt %u failed", i + 1);
    }

    //If we get here, we failed
    print(3, "touchpad_workaround: giving up after %u attempts", NUM_MAJOR_LOOP_TRIES);
    return 1;
}

/* Static Function Implementations */

static bool is_root(void) {
    return getuid() == 0;
}

static void print(uint32_t log_level, const char* format_str, ...) {
    va_list argptr;

    //Log to console
    va_start(argptr, format_str);
    vfprintf(stderr, format_str, argptr);
    va_end(argptr);
    fputc('\n', stderr);

    //Log to kernel ring buffer
    FILE* kernel_ring_buffer = fopen("/dev/kmsg", "w");
    assert(kernel_ring_buffer);
    va_start(argptr, format_str);
    fprintf(kernel_ring_buffer, "<%u>", log_level);
    vfprintf(kernel_ring_buffer, format_str, argptr);//TODO log severity
    va_end(argptr);
    fputc('\n', kernel_ring_buffer);
    fclose(kernel_ring_buffer);
}

static bool attempt(void) {
    //TODO also check if touchpad is "normal" or with weird acceleration somehow (how to do this?)

    //After waiting a bit, try to unbind the touchpad from the i2c driver
    bool failed = true;//Failed until proven not failed
    sleep_ms(MAJOR_DELAYS_MS);
    print(6, "touchpad_workaround: beginning unbind loop");

    FILE* i2c_unbind = fopen("/sys/bus/i2c/drivers/i2c_hid_acpi/unbind", "w");
    for (uint32_t i = 0; i < NUM_MINOR_LOOP_TRIES; ++i) {
        fputs(TOUCHPAD_I2C_STR, i2c_unbind);
        fflush(i2c_unbind);

        if (ferror(i2c_unbind) && (errno == ENODEV)) {//Got an error due to "No such device" (so it was sucessfully unbound)
            failed = false;
            break;
        }
        //Otherwise keep trying

        sleep_ms(MINOR_LOOP_DELAY_MS);
    }
    fclose(i2c_unbind);

    if (failed)
        print(3, "touchpad_workaround: unbind loop failed (couldn't unbind in %u tries)", NUM_MINOR_LOOP_TRIES);
    else
        print(6, "touchpad_workaround: unbind loop finished");

    //After waiting a bit more, try to rebind the touchpad
    failed = true;//Failed until proven not failed
    sleep_ms(MAJOR_DELAYS_MS);
    print(6, "touchpad_workaround: beginning rebind loop");

    FILE* i2c_bind = fopen("/sys/bus/i2c/drivers/i2c_hid_acpi/bind", "w");
    for (uint32_t i = 0; i < NUM_MINOR_LOOP_TRIES; ++i) {
        fputs(TOUCHPAD_I2C_STR, i2c_bind);
        fflush(i2c_bind);

        if (ferror(i2c_bind) && (errno == EBUSY)) {//Got an error due to "Device or resource busy" (so it was sucessfully rebound)
            failed = false;
            break;
        }

        sleep_ms(MINOR_LOOP_DELAY_MS);
    }
    fclose(i2c_bind);

    if (failed)
        print(3, "touchpad_workaround: rebind loop failed");
    else
        print(6, "touchpad_workaround: rebind loop finished");

    return !failed;
}

static void sleep_ms(uint64_t millis) {
    //Thanks https://stackoverflow.com/questions/1157209/is-there-an-alternative-sleep-function-in-c-to-milliseconds
    if (!millis)
        return;

    struct timespec ts = {
        millis / 1000,
        (millis % 1000) * 1000000
    };

    int nanosleep_result;
    do {
        nanosleep_result = nanosleep(&ts, &ts);
    } while (nanosleep_result && (errno == EINTR));
}
